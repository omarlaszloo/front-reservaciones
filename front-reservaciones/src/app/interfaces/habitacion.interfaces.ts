


export interface Habitacion {
  piso: string
  numero: string
  capacidad: string
  hotel: string
  costo_x_dia: string
}
