import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'
import { FormsModule } from '@angular/forms'

import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';


// Services
import { ServicesService } from './services.service'

// router
import { app_routing } from './app.routes';
import { HabitacionesComponent } from './habitaciones/habitaciones.component';
import { NavBarComponent } from './nav-bar/nav-bar.component'


@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    HabitacionesComponent,
    NavBarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    app_routing
  ],
  providers: [ServicesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
