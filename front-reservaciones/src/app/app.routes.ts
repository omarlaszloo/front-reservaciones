import { RouterModule, Routes } from '@angular/router'
import { RegisterComponent } from './register/register.component'
import { HabitacionesComponent } from './habitaciones/habitaciones.component'

const app_routes: Routes = [
  { path: 'register', component: RegisterComponent },
  { path: 'habitaciones', component: HabitacionesComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'register' }
]

export const app_routing = RouterModule.forRoot(app_routes)
