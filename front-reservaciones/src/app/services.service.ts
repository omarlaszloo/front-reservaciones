import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Hotel } from './interfaces/hoteles.interfaces'
import { Habitacion } from './interfaces/habitacion.interfaces'
import 'rxjs/Rx'

@Injectable()
export class ServicesService {

  public registerUrl:string = 'http://localhost:8000/add-hotel/'
  public HabitacionesUrl:string = 'http://localhost:8000/add-habitaciones/'

  constructor(private http:HttpClient) {

  }

  postRegister(hoteles:Hotel) {
    let formData = new FormData()
    for (let name in hoteles){
      formData.append(name, hoteles[name])
    }

    let body = formData

    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json'})
    }
    return this.http.post( this.registerUrl, body, { httpOptions } )
      .map( res => {
        console.log('res', res)

      })

  }

  postRegisterHabitaciones(habitaciones:Habitacion) {
    let formData = new FormData()
    for (let name in habitaciones){
      formData.append(name, habitaciones[name])
    }

    let body = formData

    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json'})
    }
    return this.http.post( this.HabitacionesUrl, body, { httpOptions } )
      .map( res => {
        console.log('res', res)

      })
  }
}
