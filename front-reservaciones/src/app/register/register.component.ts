import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../services.service'
import { Hotel } from '../interfaces/hoteles.interfaces'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  hoteles:Hotel = {
    name: "",
    description: "",
    calif: ""
  }

  constructor(private _registerServices: ServicesService) {

    // this._registerServices.postRegister()
  }

  ngOnInit() {
  }

  save() {
    console.log(this.hoteles)
    this._registerServices.postRegister(this.hoteles)
    .subscribe(data => {

    })
  }

}
