import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../services.service'
import { Habitacion } from '../interfaces/habitacion.interfaces'

@Component({
  selector: 'app-habitaciones',
  templateUrl: './habitaciones.component.html',
  styleUrls: ['./habitaciones.component.css']
})
export class HabitacionesComponent implements OnInit {

  habitaciones:Habitacion = {
    piso: "",
    numero: "",
    capacidad: "",
    hotel: "",
    costo_x_dia: ""

  }

  constructor(private _registerServices: ServicesService) { }

  ngOnInit() {
  }

  saveHabitacion() {
    console.log(this.habitaciones)
    this._registerServices.postRegisterHabitaciones(this.habitaciones)
    .subscribe(data => {

    })
  }

}
